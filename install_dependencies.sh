#!/bin/bash

# Update package lists and upgrade existing packages
sudo apt update
sudo apt upgrade -y

# Install Git
sudo apt install -y git

# Check if the current directory is in 'beacukai-ai-streamer-kit' or not
if [ ! -d "beacukai-ai-streamer-kit" ]; then
    # If not, clone the repository
    git clone https://gitlab.com/mufidhadi/beacukai-ai-streamer-kit.git
    # Change the working directory to 'beacukai-ai-streamer-kit'
    cd beacukai-ai-streamer-kit
fi

# Install Node.js 18 (npm is included with Node.js)
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt install -y nodejs

# Install Python 3.10 and pip
# sudo apt install -y python3.10 python3-pip

# install python 3.8
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install -y python3.8 python3-pip

# Install Python 3 venv
# sudo apt install -y python3-venv
sudo apt install -y python3.8-venv

# Install MySQL Server
sudo apt install -y mysql-server

# Start and enable MySQL service
sudo systemctl start mysql
sudo systemctl enable mysql

# Secure MySQL installation (set root password and remove anonymous users)
sudo mysql_secure_installation

sudo mysql<<MYSQL_SCRIPT
SET GLOBAL validate_password.LENGTH = 4;
SET GLOBAL validate_password.policy = 0;
SET GLOBAL validate_password.mixed_case_count = 0;
SET GLOBAL validate_password.number_count = 0;
SET GLOBAL validate_password.special_char_count = 0;
SET GLOBAL validate_password.check_user_name = 0;
FLUSH PRIVILEGES;
MYSQL_SCRIPT

# Create a MySQL user 'ai' with password 'Makanmalam1930', grant all privileges, and allow remote connections
sudo mysql<<MYSQL_SCRIPT
CREATE USER 'ai'@'%' IDENTIFIED WITH mysql_native_password BY 'Makanmalam1930';
GRANT ALL PRIVILEGES ON *.* TO 'ai'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
MYSQL_SCRIPT

# Install ffmpeg
sudo apt install -y ffmpeg

# Install vsftpd (FTP server)
sudo apt install -y vsftpd

# Start and enable vsftpd service
sudo systemctl start vsftpd
sudo systemctl enable vsftpd

# Install CUDA
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.0-1_all.deb
sudo dpkg -i cuda-keyring_1.0-1_all.deb
sudo apt update
sudo apt-get -y install cuda

echo "Installation complete: Git, Node.js 18 (including npm), Python 3.10, pip, venv, MySQL Server, ffmpeg, vsftpd, CUDA, and MySQL user 'ai' with remote access are configured."

echo "main code setup"

# install PM2
sudo npm install -g pm2
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u juaralogistik --hp /home/juaralogistik

# PM2 manager
cd pm2_manager
npm install
pm2 start index.js --name pm2_mgr --watch
pm2 save

cd ..

# rtsp MJPEG
cd rtsp_mjpeg
# python3.10 -m venv venv
python3.8 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
pm2 start main.py --interpreter=python --name mjpeg
pm2 save
deactivate

cd ..

# rtsp server
cd yolov8-tracking
chmod +x mediamtx
pm2 start mediamtx --name rtsp
pm2 save

# setup database
sudo mysql < release_db.sql

# setup venv
# python3.10 -m venv venv
python3.8 -m venv venv
source venv/bin/activate

# install library
pip install -r requirements_final.txt

# setup storage manager
pm2 start storage_manager.py --interpreter=python --name=storage-mgr --cron-restart="0 0 * * *"
pm2 save
deactivate

cd ..

# UI backend
cd streamer_ui/backend/
npm install
pm2 start index.js --name=ui_backend --watch
pm2 save

cd ..
cd ..

# UI frontend
cd streamer_ui/frontend/
pm2 serve build/ 3000 --name ui_frontend --spa --watch
pm2 save

cd ..
cd ..

echo "done code setup"


# allow firewall
echo "setting UFW firewalls"

sudo ufw allow 3000
sudo ufw allow 3333
sudo ufw allow 3334
sudo ufw allow 3335
sudo ufw allow 3336
sudo ufw allow 5000
sudo ufw allow 5555
sudo ufw allow 5556
sudo ufw allow 6379
sudo ufw allow 8554
sudo ufw allow rtsp
sudo ufw allow mysql
sudo ufw allow ssh
sudo ufw enable

echo "done firewall setting"