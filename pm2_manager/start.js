const { exec } = require('child_process');
const path = require('path');
const os = require('os');

// Define the path to your Python script
const pythonScriptPath = path.join('..', 'yolov8-tracking', 'main.py');

// Define the command-line arguments
const pythonScriptArgs = '-i 1';

// Determine the virtual environment activation script based on the operating system
let venvActivateScript;
if (os.platform() === 'win32') {
  venvActivateScript = path.join('..', 'yolov8-tracking', 'venv', 'Scripts', 'activate.bat');
} else {
  venvActivateScript = path.join('..', 'yolov8-tracking', 'venv', 'bin', 'activate');
}

// Command to activate the virtual environment and run the Python script
const command = `"${venvActivateScript}" && python "${pythonScriptPath}" ${pythonScriptArgs}`;

const child = exec(command);

child.stdout.on('data', (data) => {
  console.log(`stdout: ${data}`);
});

child.stderr.on('data', (data) => {
  console.error(`stderr: ${data}`);
});

child.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});
