const express = require('express');
const pm2 = require('pm2');
const mysql = require('mysql');
const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path')
const { exec } = require('child_process');
const cors = require('cors');

const http = require('http');
const WebSocket = require('ws');

const os = require('os');

// Load MySQL settings from config.yaml
const config = yaml.load(fs.readFileSync('config.yaml', 'utf8'));
const mysqlSetting = config.mysql;

const connection = mysql.createConnection(mysqlSetting);

connection.connect((err) => {
    if (err) {
        console.error('Error connecting to MySQL database:', err);
        return;
    }
    console.log('Connected to MySQL database');
});

const app = express();
const PORT = 5555; // Change this to the desired port number
const PORT_WS = 5556; // Change this to the desired port number

const server = http.createServer(app); // Create an HTTP server
const wss = new WebSocket.Server({ server }); // Create a WebSocket server

app.use(express.json());
app.use(cors());

app.get('/restart/:pm2_id', (req, res) => {
    const pm2_id = req.params.pm2_id;

    pm2.connect((err) => {
        if (err) {
            console.error('PM2 connection error:', err.message);
            return res.status(500).send('Error connecting to PM2');
        }

        pm2.restart(pm2_id, (restartErr) => {
            pm2.disconnect();

            if (restartErr) {
                console.error(`Error restarting PM2 process ${pm2_id}:`, restartErr.message);
                return res.status(500).send(`Error restarting PM2 process ${pm2_id}`);
            }

            res.send(`PM2 process ${pm2_id} restarted successfully`);
        });
    });
});

app.get('/restart_cam/:cam_id', (req, res) => {
    const cam_id = req.params.cam_id

    const query = 'SELECT * FROM kamera WHERE id_dashboard=' + cam_id;
    connection.query(query, (err, results) => {
        if (err) {
            console.error('Error executing query:', err);
            return;
        }
        console.log('Fetched data:', results);
        // res.send(results)
        // close_mysql();
        if (results.length > 0) {
            const kamera = results[0]
            const pm2_id = 'cam' + kamera.id

            pm2.connect((err) => {
                if (err) {
                    console.error('PM2 connection error:', err.message);
                    return res.status(500).send('Error connecting to PM2');
                }

                pm2.restart(pm2_id, (restartErr) => {
                    pm2.disconnect();

                    if (restartErr) {
                        console.error(`Error restarting PM2 process ${pm2_id}:`, restartErr.message);
                        return res.status(500).send(`Error restarting PM2 process ${pm2_id}: ${restartErr.message}`);
                    }

                    res.send(`PM2 process ${pm2_id} restarted successfully`);
                });
            });
        }
    })
})

app.get('/list', (req, res) => {
    pm2.connect((err) => {
        if (err) {
            console.error('PM2 connection error:', err.message);
            return res.status(500).json({ error: 'Error connecting to PM2' });
        }

        pm2.list((listErr, processList) => {
            pm2.disconnect();

            if (listErr) {
                console.error('Error listing PM2 processes:', listErr.message);
                return res.status(500).json({ error: 'Error listing PM2 processes' });
            }

            res.json({ processes: processList });
        });
    });
});

app.get('/create/:cam_id', (req, res) => {
    const cam_id = req.params.cam_id

    pm2.connect((err) => {
        if (err) {
            console.error('PM2 connection error:', err.message);
            return res.status(500).json({ error: 'Error connecting to PM2' });
        }

        pm2.start({
            script: './start_yolo.js',
            args: '-i ' + cam_id + ' -r True',
            name: 'cam' + cam_id,
        }, (pm2Err, pm2Proc) => {
            pm2.disconnect()
            if (pm2Err) {
                console.log('🪲 error pm2 start', pm2Err.message)
                return res.status(500).send(`Error starting PM2 process: ${pm2Err.message}`)
            }
            return res.json({ result: pm2Proc })
        })
    });
});

app.get('/stop/:pm2_id', (req, res) => {
    const pm2_id = req.params.pm2_id;
    console.log(pm2_id, (typeof pm2_id));

    pm2.connect((err) => {
        if (err) {
            console.error('PM2 connection error:', err.message);
            return res.status(500).send('Error connecting to PM2');
        }

        pm2.stop(pm2_id, (stopErr) => {
            pm2.disconnect();

            if (stopErr) {
                console.error(`Error stoping PM2 process ${pm2_id}:`, stopErr.message);
                return res.status(500).send(`Error stoping PM2 process ${pm2_id}`);
            }

            res.send(`PM2 process ${pm2_id} stoped successfully`);
        });
    });
});

app.get('/delete/:pm2_id', (req, res) => {
    const pm2_id = req.params.pm2_id;

    pm2.connect((err) => {
        if (err) {
            console.error('PM2 connection error:', err.message);
            return res.status(500).send('Error connecting to PM2');
        }

        pm2.delete(pm2_id, (deleteErr) => {
            pm2.disconnect();

            if (deleteErr) {
                console.error(`Error deleting PM2 process ${pm2_id}:`, deleteErr.message);
                return res.status(500).send(`Error deleting PM2 process ${pm2_id}`);
            }

            res.send(`PM2 process ${pm2_id} deleteed successfully`);
        });
    });
});

let sendWsInterval

// WebSocket route to list PM2 processes (for WebSocket connections)
wss.on('connection', (ws) => {
    console.log('WebSocket client connected')
    wsSendPm2List(ws);
    sendWsInterval = setInterval(()=>{wsSendPm2List(ws)},1000)
    
    ws.on('close',()=>{
        console.log('WebSocket client disconnected')

        clearInterval(sendWsInterval)
    })
    
    ws.on('error',(wsErr)=>{
        console.log('WebSocket client error',wsErr)
    
        clearInterval(sendWsInterval)
    })
});

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});

server.listen(PORT_WS, () => {
    console.log(`Server is listening on port ${PORT_WS}`);
});

function wsSendPm2List(ws) {
    pm2.connect((err) => {
        if (err) {
            console.error('PM2 connection error:', err.message);
            return ws.send(JSON.stringify({ error: 'Error connecting to PM2' }));
        }

        pm2.list((listErr, processList) => {
            pm2.disconnect();

            if (listErr) {
                console.error('Error listing PM2 processes:', listErr.message);
                return ws.send(JSON.stringify({ error: 'Error listing PM2 processes' }));
            }

            let memory_usage = []
            const freemem  = os.freemem()
            const totalmem  = os.totalmem()
            let totalPm2mem = 0

            // memory_usage.push({
            //     name:'free',
            //     memory_usage: freemem,
            // })

            processList.map((pm)=>{
                totalPm2mem += pm.monit.memory
                memory_usage.push({
                    name:pm.name,
                    memory_usage: pm.monit.memory,
                })
            })
            
            const othermem = totalmem - freemem - totalPm2mem

            // memory_usage.push({
            //     name:'other',
            //     memory_usage: othermem,
            // })

            ws.send(JSON.stringify({ processes: processList, freemem, totalmem, memory_usage }));
        });
    });
}

function get_kamera(cam_id) {
}

function close_mysql() {
    connection.end((err) => {
        if (err) {
            console.error('Error closing connection:', err);
            return;
        }
        console.log('Connection to MySQL database closed');
    });
}

