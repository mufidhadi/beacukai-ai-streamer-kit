const { exec } = require('child_process')
const path = require('path')
const os = require('os')

const pythonScriptPath = path.join('..', 'yolov8-tracking', 'main.py')
// const pythonScriptArgs = '-i 1'
const pythonScriptArgs = process.argv.slice(2).join(' ')

// Determine the virtual environment activation script based on the operating system
let venvActivateScript
let command = ``
if (os.platform() === 'win32') {
  venvActivateScript = path.join('..', 'yolov8-tracking', 'venv', 'Scripts', 'activate.bat')
  command = `"${venvActivateScript}" && python "${pythonScriptPath}" ${pythonScriptArgs}`
} else {
  venvActivateScript = path.join('..', 'yolov8-tracking', 'venv', 'bin', 'activate')
  command = `source ${venvActivateScript} && python "${pythonScriptPath}" ${pythonScriptArgs}`
}

// Command to activate the virtual environment and run the Python script
console.log('command >>>>',command);

const child = exec(command)

child.stdout.on('data', (data) => {
  console.log(`stdout: ${data}`)
})

child.stderr.on('data', (data) => {
  console.error(`stderr: ${data}`)
})

child.on('close', (code) => {
  console.log(`child process exited with code ${code}`)
})

// Listen for SIGINT (Ctrl+C) signal and stop the child process
process.on('SIGINT', () => {
  console.log('Received SIGINT. Stopping the child process...');
  child.kill('SIGINT'); // Send SIGINT to the child process
});