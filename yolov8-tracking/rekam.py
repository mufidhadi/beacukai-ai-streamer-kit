import cv2
from ultralytics import YOLO
import supervision as sv
import subprocess as sp
import mysql.connector
import threading
import utils.config as cf
import argparse
from prettytable import PrettyTable
import os
import json

# TODO: multi tasking fungsi run

ids = []
counter_objek = {}
counter_objek_in = {}
counter_objek_out = {}
counter_objek_in_keys = []
counter_objek_out_keys = []
detected_obj = list()
# counter_in = 0
# counter_out = 0

# allowed_cls = ['person','car','motorcycle','bus','truck']
# allowed_cls = ['car','motorcycle','bus','truck']
# allowed_cls = ['mobil','motor','bus','truck','pickup','becak','orang','plat_nomor']
# allowed_cls = ['paket']
allowed_cls = ['kayu']
allowed_cls_str = ', '.join(allowed_cls)


# koordinat garis counting objek
START = sv.Point(320, 0)
END = sv.Point(320, 480)

rtsp_server = cf.config['rtsp']['base_url']

size_str = '640x480'
fps = 24

FOLDER = 'video'
FOLDER_OTPUT = 'video_out'

def run(file_path='RUAS_Ir._H._Djuanda-08.05.2023-07.54.37.mp4',all_cls=False):
    file_path_full = os.path.join(FOLDER,file_path)
    counter_in = 0
    counter_out = 0
    last_line_states = []

    # ambil ukuran video
    cap = cv2.VideoCapture(file_path_full)
    video_h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    video_w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    # fourcc = cv2.VideoWriter_fourcc(cap.get(cv2.CAP_PROP_FOURCC))
    size_str = str(video_w)+'x'+str(video_h)
    fps = cap.get(cv2.CAP_PROP_FPS)

    ukuran = (video_w, video_h)
    # output_dir = 'output_video_ruas_detektor'
    output_dir = FOLDER_OTPUT
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    output_path = os.path.join(output_dir,file_path)
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    rekaman = cv2.VideoWriter(output_path,fourcc,fps, ukuran)

    # bikin start dan end garis jadi horizontal selebar video
    START = sv.Point(0, int(video_h/2))
    END = sv.Point(int(video_w), int(video_h/2))
    # START = sv.Point(int(video_w/2), int(video_h))
    # END = sv.Point(int(video_w/2), int(0))

    model = YOLO(cf.config['yolo']['model'])

    line_zone = sv.LineZone(start=START, end=END)
    line_zone_annotator = sv.LineZoneAnnotator(
        thickness=2,
        text_thickness=1,
        text_scale=0.5
    )

    box_anotator = sv.BoxAnnotator(
        thickness=2,
        text_thickness=1,
        text_scale=0.5
    )

    for result in model.track(source=file_path_full, show=False, stream=True):
    # for result in model.track(source=file_path, show=False, stream=True, tracker="bytetrack.yaml"):
        frame = result.orig_img
  
        detections = sv.Detections.from_yolov8(result)

        if result.boxes.id is not None:
            detections.tracker_id = result.boxes.id.cpu().numpy().astype(int)
            # print('detection:',detections)

            temp_detections = []
            for det in detections:
                # print('det',det)
                track_id = int(det[3])
                cls_id = int(det[2])
                name = model.model.names[cls_id]
                if track_id not in ids:
                    ids.append(track_id)
                    # if not all_cls:
                    #     if name in allowed_cls:
                    #         detected_obj.append({
                    #             'track_id':track_id,'cls_id':cls_id,'name':name,'direction':''
                    #         })
                    # else:
                    #     detected_obj.append({
                    #         'track_id':track_id,'cls_id':cls_id,'name':name,'direction':''
                    #     })
                    if name not in counter_objek:
                        if not all_cls:
                            if name in allowed_cls:
                                counter_objek[name] = 1
                        else:
                            counter_objek[name] = 1
                    else:
                        if not all_cls:
                            if name in allowed_cls:
                                counter_objek[name] += 1
                        else:
                            counter_objek[name] += 1
                    print('counter:',counter_objek)
                if not all_cls:
                    if name in allowed_cls:
                        temp_detections.append(det)
            if not all_cls:
                detections = temp_detections


            # detections = detections[detections.class_id != 0] #memfilter agar class 0 tidak ditampilkan di frame
            # detections = detections[class_id in [0,2,3,5,7] for class_id in detections.class_id] #filter nama (tes)
            # detections = detections[model.model.names[detections.class_id] in allowed_cls] #filter nama (tes)

            labels = [
                f'id:{tracker_id} {model.model.names[class_id]} ({confidence:0.2f})'
                for _, confidence, class_id, tracker_id
                in detections
            ]

            frame = box_anotator.annotate(scene=frame, detections=detections, labels=labels)

            last_line_states = line_zone.tracker_state.copy()
            # last_line_states = line_zone.tracker_state
            line_zone.trigger(detections=detections)
            line_zone_annotator.annotate(frame=frame, line_counter=line_zone)


        print('in:',line_zone.in_count)
        print('out:',line_zone.out_count)
        # print('state:',line_zone.tracker_state)

        if len(line_zone.tracker_state) > 0 and len(last_line_states) > 0:
            keys = list(line_zone.tracker_state.keys())
            # print(keys, type(keys))
            # print(last_line_states, type(last_line_states))

            if line_zone.in_count > counter_in or line_zone.out_count > counter_out:
                for k in keys:
                    # print(k,last_line_states[k],line_zone.tracker_state[k])
                    # print('🔍🔍🔍 k in last_line_states',(k in last_line_states))
                    if k in last_line_states and last_line_states[k] != line_zone.tracker_state[k] :
                        print('CEK =====>',k,last_line_states[k],line_zone.tracker_state[k])

                        obj_name = ''
                        for det in detections:
                            if det[3] is not None:
                                track_id = int(det[3])
                                cls_id = int(det[2])
                                name = model.model.names[cls_id]
                                if int(track_id) == int(k):
                                    obj_name = name

                        if obj_name != '':
                            if line_zone.tracker_state[k] :
                                counter_in += 1
                                counter_objek_in_keys.append(k)
                                if obj_name not in counter_objek_in:
                                    counter_objek_in[obj_name] = 1
                                else:
                                    counter_objek_in[obj_name] += 1
                            else :
                                counter_out += 1
                                counter_objek_out_keys.append(k)
                                if obj_name not in counter_objek_out:
                                    counter_objek_out[obj_name] = 1
                                else:
                                    counter_objek_out[obj_name] += 1
                            break
                # break
            
        # last_line_states = line_zone.tracker_state

        
            
        # print('in:',counter_objek_in_keys)
        # print('in:',counter_objek_in)
        # print('out:',counter_objek_out_keys)
        # print('out:',counter_objek_out)

        # TEXT COUNTER TOTAL

        text_counter = json.dumps(counter_objek)

        display = frame.copy()
        h, w = frame.shape[0], frame.shape[1]
        x1 = 10
        y1 = 10
        x2 = 10
        y2 = 70

        # txt_size = cv2.getTextSize(str(text_counter), cv2.FONT_HERSHEY_SIMPLEX, 0.55, 1)[0]
        # cv2.rectangle(frame, (x1, y1 + 1), (txt_size[0] * 2, y2),(0, 0, 0),-1)
        # cv2.putText(frame, str(text_counter), (x1 + 10, y1 + 35), cv2.FONT_HERSHEY_SIMPLEX,1.0, (210, 210, 210), 2)
        # cv2.addWeighted(frame, 1.0, display, 1 - 0.7, 0, frame)

        # TEXT COUNTER IN

        text_counter = json.dumps(counter_objek_in)

        display = frame.copy()
        h, w = frame.shape[0], frame.shape[1]
        x1 = 10 + int(video_w/2)
        y1 = 10 + int(video_h/2) - 100
        x2 = 10 + int(video_w/2)
        y2 = 70 + int(video_h/2) - 100

        # txt_size = cv2.getTextSize(str(text_counter), cv2.FONT_HERSHEY_SIMPLEX, 0.55, 1)[0]
        # cv2.rectangle(frame, (x1, y1 + 1), (txt_size[0] * 2, y2),(0, 0, 0),-1)
        # cv2.putText(frame, str(text_counter), (x1 + 10, y1 + 35), cv2.FONT_HERSHEY_SIMPLEX,1.0, (210, 210, 210), 2)
        # cv2.addWeighted(frame, 1.0, display, 1 - 0.7, 0, frame)

        # TEXT COUNTER OUT

        text_counter = json.dumps(counter_objek_out)

        display = frame.copy()
        h, w = frame.shape[0], frame.shape[1]
        x1 = 10 + int(video_w/2)
        y1 = 10 + int(video_h/2)
        x2 = 10 + int(video_w/2)
        y2 = 70 + int(video_h/2)

        # txt_size = cv2.getTextSize(str(text_counter), cv2.FONT_HERSHEY_SIMPLEX, 0.55, 1)[0]
        # cv2.rectangle(frame, (x1, y1 + 1), (txt_size[0] * 2, y2),(0, 0, 0),-1)
        # cv2.putText(frame, str(text_counter), (x1 + 10, y1 + 35), cv2.FONT_HERSHEY_SIMPLEX,1.0, (210, 210, 210), 2)
        # cv2.addWeighted(frame, 1.0, display, 1 - 0.7, 0, frame)
        

        cv2.imshow(file_path,frame)
        if(cv2.waitKey(30)==27):
            break

        rekaman.write(frame)

def opt_parse():
    parser = argparse.ArgumentParser(
        description='Program Object detection untuk mendeteksi, mengklasisfikasi, dan menghitung kendaraan dari tangkapan RTSP CCTV',
    )
    parser.add_argument('--id','-i',type=int,default=None,help='id kamera dari tabel kamera di database')
    parser.add_argument('--all','-a',type=bool,default=False,help='mendeteksi semua class, tidak hanya '+allowed_cls_str)
    opt = parser.parse_args()
    return opt

def main():
    opt = opt_parse()

    file_list = os.listdir(FOLDER)

    if opt.id is not None:
        run(file_list[int(opt.id)],opt.all)
    else:
        tabel = PrettyTable()
        tabel.field_names = ['id', 'file']
        i = 0
        for file in file_list :
            tabel.add_row([i,file])
            i+=1
        print('Daftar file: ')
        print(tabel)


if __name__=='__main__':
    main()