import cv2
from ultralytics import YOLO
import supervision as sv
import subprocess as sp
import mysql.connector
import threading
import utils.config as cf
import argparse
from prettytable import PrettyTable
import os
import json



# koordinat garis counting objek
START = sv.Point(320, 0)
END = sv.Point(320, 480)

rtsp_server = cf.config['rtsp']['base_url']

size_str = '640x480'
fps = 24

FOLDER = 'video'
FOLDER_OTPUT = 'video_out'

def run(file_path='RUAS_Ir._H._Djuanda-08.05.2023-07.54.37.mp4',all_cls=False):
    file_path_full = os.path.join(FOLDER,file_path)
    rtsp_server_run = rtsp_server+file_path

    # ambil ukuran video
    cap = cv2.VideoCapture(file_path_full)
    video_h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    video_w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    # fourcc = cv2.VideoWriter_fourcc(cap.get(cv2.CAP_PROP_FOURCC))
    size_str = str(video_w)+'x'+str(video_h)
    fps = cap.get(cv2.CAP_PROP_FPS)

    command = ['ffmpeg',
        '-re',
        '-s', size_str,
        '-r', str(fps),  # rtsp fps (from input server)
        '-i', '-',
            
        # You can change ffmpeg parameter after this item.
        '-pix_fmt', 'yuv420p',
        '-r', '30',  # output fps
        '-g', '50',
        '-c:v', 'libx264',
        '-b:v', '2M',
        '-bufsize', '64M',
        '-maxrate', "4M",
        '-preset', 'veryfast',
        '-rtsp_transport', 'tcp',
        '-segment_times', '5',
        '-f', 'rtsp',
        rtsp_server_run
    ]

    process = sp.Popen(command, stdin=sp.PIPE)
    
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        try:
            process.stdin.write(frame.tobytes())
        except:
            print('error kirim RTSP')
            exit()
    cap.release()

def opt_parse():
    parser = argparse.ArgumentParser(
        description='Program Object detection untuk mendeteksi, mengklasisfikasi, dan menghitung kendaraan dari tangkapan RTSP CCTV',
    )
    parser.add_argument('--id','-i',type=int,default=None,help='id kamera dari tabel kamera di database')
    parser.add_argument('--all','-a',type=bool,default=False,help='mendeteksi semua class, tidak hanya '+allowed_cls_str)
    opt = parser.parse_args()
    return opt

def main():
    opt = opt_parse()

    file_list = os.listdir(FOLDER)

    if opt.id is not None:
        run(file_list[int(opt.id)],opt.all)
    else:
        tabel = PrettyTable()
        tabel.field_names = ['id', 'file']
        i = 0
        for file in file_list :
            tabel.add_row([i,file])
            i+=1
        print('Daftar file: ')
        print(tabel)


if __name__=='__main__':
    main()