import os
import yaml

default_config = {
    'mysql':{
        'host': 'localhost',
        'user': 'root',
        'password': '',
        'database': 'strongshort_cctv'
    },
    'rtsp':{
        'base_url':'rtsp://localhost:8554/mufid/'
    },
    'yolo':{
        'model':'yolov8s.pt'
    },
    'redis':{
        'host':'localhost',
        'port':6379
    },
    'api':{
        'url':'http://36.93.9.122:8080/beacukai_camera/'
    },
    'device_vpn_ip':'172.0.0.1',
    'delete_treshold_gb':100,
    'file_keep_ratio':1.75,
}

config_yaml_path = 'config.yaml'

config = default_config
if os.path.exists(config_yaml_path):
    config = yaml.safe_load(open(config_yaml_path))
else :
    yaml.dump(default_config, open(config_yaml_path, 'w'))