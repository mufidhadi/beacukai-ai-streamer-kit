import cv2
from ultralytics import YOLO
import supervision as sv
import subprocess as sp
import mysql.connector
import utils.config as cf
import argparse
from prettytable import PrettyTable
import os
import json
# import redis
import requests
import time
import numpy

# TODO: multi tasking fungsi run

mydb = mysql.connector.connect(
  host=cf.config['mysql']['host'],
  user=cf.config['mysql']['user'],
  password=cf.config['mysql']['password'],
  database=cf.config['mysql']['database'],
)

ids = []
counter_objek = {}
counter_objek_in = {}
counter_objek_out = {}
counter_objek_in_keys = []
counter_objek_out_keys = []
detected_obj = list()

allowed_cls = ['person','car','motorcycle','bus','truck','trucks']
allowed_cls_str = ', '.join(allowed_cls)

# koordinat garis counting objek
START = sv.Point(320, 0)
END = sv.Point(320, 480)

rtsp_server = cf.config['rtsp']['base_url']

size_str = '640x480'
fps = 24

FOLDER_OTPUT = 'video_out'

# redis
# r = redis.Redis(host=cf.config['redis']['host'],port=cf.config['redis']['port'])

# HTTP-API
http_url = cf.config['api']['url']

rekaman = None

def ambil_garis_di_server(cam_id,video_h,video_w):
    start = sv.Point(0, int(video_h/2))
    end = sv.Point(int(video_w), int(video_h/2))

    try:
        api_params = {
            'camera_id':str(cam_id),
        }
        req = requests.get(url=http_url+'api/Detectors/line',params=api_params)
        if req.status_code == 200:
            data = req.json()
            print('👉👉👉',data)
            start = sv.Point(float(data['point_a_x']) * float(video_w),float(data['point_a_y']) * float(video_h))
            end = sv.Point(float(data['point_b_x']) * float(video_w),float(data['point_b_y']) * float(video_h))
            return start, end
        else:
            print('tida bicaa')
    except Exception as e:
        print('erornya kakaaa... 😉👉',e)
    pass

def prepare_rekaman(cam_id, start_time, video_w, video_h):
    # file_path = 'video_'+str(id)+'.mp4'
    # file_path = 'video_'+str(id)+'_'+str(start_time)+'.avi'
    file_path = 'video_'+str(cam_id)+'_'+str(start_time)+'.avi'
    ukuran = (video_w, video_h)
    # output_dir = 'output_video_ruas_detektor'
    output_dir = FOLDER_OTPUT
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    output_path = os.path.join(output_dir,file_path)
    # fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    # fourcc = cv2.VideoWriter_fourcc(*'DIVX')
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    rekaman = cv2.VideoWriter(output_path,fourcc,fps, ukuran)
    return rekaman

def run(id,all_cls=False,rekam=False,cam_id=1,durasi_rekam=10):
    start_time = time.time()
    start_time_rekaman = time.time()
    past_time_rekaman = start_time_rekaman
    nomor_frame = 0
    
    print(cam_id)
    redis_topic = 'cam/'+str(id)
    # r.publish(redis_topic,'start')
    
    counter_in = 0
    counter_out = 0
    last_line_states = []

    id = str(id)
    rtsp_server_run = rtsp_server+str(id)
    mycursor = mydb.cursor()
    mycursor.execute('select * from kamera where id='+id)
    myresult = mycursor.fetchone()
    print(myresult)
    rtsp_address = myresult[4]
    print('[START!] ambil rtsp:',rtsp_address)
    try:
        cam_id_db = myresult[5]
        if (cam_id_db != None) and (int(cam_id_db) > 0): 
            cam_id = cam_id_db
    except:
        pass
    
    mycursor.close()

    # ambil ukuran video
    cap = cv2.VideoCapture(rtsp_address)
    video_h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    video_w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    size_str = str(video_w)+'x'+str(video_h)
    fps = cap.get(cv2.CAP_PROP_FPS)

    if rekam:
        rekaman = prepare_rekaman(cam_id, start_time, video_w, video_h)

    try:
        START, END = ambil_garis_di_server(cam_id,video_h,video_w)
    except:
        # bikin start dan end garis jadi horizontal selebar video
        START = sv.Point(0, int(video_h/2))
        END = sv.Point(int(video_w), int(video_h/2))

    command = ['ffmpeg',
        '-re',
        '-s', size_str,
        '-r', str(fps),  # rtsp fps (from input server)
        '-i', '-',
            
        # You can change ffmpeg parameter after this item.
        '-pix_fmt', 'yuv420p',
        '-r', '24',  # output fps
        # '-r', str(fps),  # output fps
        '-g', '50',
        '-c:v', 'libx264',
        '-b:v', '2M',
        '-bufsize', '64M',
        '-maxrate', "4M",
        '-preset', 'veryfast',
        '-rtsp_transport', 'tcp',
        '-segment_times', '5',
        '-f', 'rtsp',
        rtsp_server_run
    ]

    # process = sp.Popen(command, stdin=sp.PIPE, shell=True)
    process = sp.Popen(command, stdin=sp.PIPE)

    model = YOLO(cf.config['yolo']['model'])

    line_zone = sv.LineZone(start=START, end=END)
    line_zone_annotator = sv.LineZoneAnnotator(
        thickness=2,
        text_thickness=1,
        text_scale=0.5
    )

    box_anotator = sv.BoxAnnotator(
        thickness=2,
        text_thickness=1,
        text_scale=0.5
    )

    # for result in model.track(source=rtsp_address, show=False, stream=True, tracker="bytetrack.yaml"):
    for result in model.track(source=rtsp_address, show=False, stream=True, tracker="bytetrack.yaml",imgsz=320):
        frame = result.orig_img

        detections = sv.Detections.from_yolov8(result)
        # detections = sv.Detections.from_ultralytics(result)

        if result.boxes.id is not None:
            detections.tracker_id = result.boxes.id.cpu().numpy().astype(int)
            labels = []
            # print('A detection:',detections)

            temp_detections = []
            for det in detections:
                # print('det',det)
                track_id = int(det[3])
                cls_id = int(det[2])
                name = model.model.names[cls_id]
                if track_id not in ids:
                    count_tmp = 0
                    ids.append(track_id)
                    if name not in counter_objek:
                        if not all_cls:
                            if name in allowed_cls:
                                counter_objek[name] = 1
                        else:
                            counter_objek[name] = 1
                    else:
                        count_tmp = counter_objek[name] + 1
                        if not all_cls:
                            if name in allowed_cls:
                                counter_objek[name] += 1
                        else:
                            counter_objek[name] += 1
                    print('counter:',counter_objek)

                    # # REST-API
                    # api_params = {
                    #     'camera_id':str(cam_id),
                    #     'object':name,
                    #     'counter':str(count_tmp),
                    #     # 'counter':str(counter_objek[name]),
                    # }
                    # req = requests.get(url=http_url+'api/Detectors/log_deteksi',params=api_params)

                if not all_cls:
                    if name in allowed_cls:
                        temp_detections.append(det)

            # if not all_cls:
            #     tmp_xyxy = []
            #     tmp_confidence = []
            #     tmp_class_id = []
            #     tmp_tracker_id = []
                
            #     tmp_xyxy_idx = 0
            #     tmp_confidence_idx = 2
            #     tmp_class_id_idx = 3
            #     tmp_tracker_id_idx = 4

            #     if len(temp_detections) > 1:
            #         if len(temp_detections[0]) < 5:
            #             tmp_xyxy_idx = 0
            #             tmp_confidence_idx = 1
            #             tmp_class_id_idx = 2
            #             tmp_tracker_id_idx = 3
                
            #         for tmp_det in temp_detections:
            #             tmp_xyxy.append(tmp_det[tmp_xyxy_idx])
            #             tmp_confidence.append(tmp_det[tmp_confidence_idx])
            #             tmp_class_id.append(tmp_det[tmp_class_id_idx])
            #             tmp_tracker_id.append(tmp_det[tmp_tracker_id_idx])
            #             labels.append('id:'+str(tmp_det[tmp_tracker_id_idx])+' '+str(model.model.names[int(tmp_det[tmp_class_id_idx])])+f' ({tmp_det[tmp_confidence_idx]:0.2f})')
                    
            #         tmp_xyxy = numpy.array(tmp_xyxy)
            #         tmp_confidence = numpy.array(tmp_confidence)
            #         tmp_class_id = numpy.array(tmp_class_id)
            #         tmp_tracker_id = numpy.array(tmp_tracker_id)

            #         detections = sv.Detections(xyxy=tmp_xyxy,confidence=tmp_confidence,class_id=tmp_class_id,tracker_id=tmp_tracker_id)
            # else:
            #     labels = [
            #         f'id:{tracker_id} {model.model.names[class_id]} ({confidence:0.2f})'
            #         for _, confidence, class_id, tracker_id
            #         in detections
            #     ]
            # r.publish(redis_topic,json.dumps(counter_objek))


            # detections = detections[detections.class_id != 0] #memfilter agar class 0 tidak ditampilkan di frame

            # try:
            #     labels_ori = [
            #         f'id:{tracker_id} {model.model.names[class_id]} ({confidence:0.2f})'
            #         for _, confidence, class_id, tracker_id
            #         in detections
            #     ]
            #     labels = labels_ori

            # except Exception as e:
            #     # labels = []
            #     print('error labels', e)
            
            labels_ori = [
                f'id:{tracker_id} {model.model.names[class_id]} ({confidence:0.2f})'
                for _, confidence, class_id, tracker_id
                in detections
            ]
            labels = labels_ori

            frame = box_anotator.annotate(scene=frame, detections=detections, labels=labels)
            # except:
                # print('error labels')

            last_line_states = line_zone.tracker_state.copy()

        line_zone.trigger(detections=detections)
        line_zone_annotator.annotate(frame=frame, line_counter=line_zone)

        print('in:',line_zone.in_count)
        print('out:',line_zone.out_count)
        # print('state:',line_zone.tracker_state)

        if len(line_zone.tracker_state) > 0 and len(last_line_states) > 0:
            keys = list(line_zone.tracker_state.keys())
            # print(keys, type(keys))
            # print(last_line_states, type(last_line_states))

            if line_zone.in_count > counter_in or line_zone.out_count > counter_out:
                for k in keys:
                    # print(k,last_line_states[k],line_zone.tracker_state[k])
                    # print('🔍🔍🔍 k in last_line_states',(k in last_line_states))
                    if k in last_line_states and last_line_states[k] != line_zone.tracker_state[k] :
                        print('CEK =====>',k,last_line_states[k],line_zone.tracker_state[k])

                        obj_name = ''
                        for det in detections:
                            if det[3] is not None:
                                track_id = int(det[3])
                                cls_id = int(det[2])
                                name = model.model.names[cls_id]
                                if int(track_id) == int(k):
                                    obj_name = name

                        if obj_name != '':
                            if line_zone.tracker_state[k] :
                                counter_in += 1
                                counter_objek_in_keys.append(k)
                                if obj_name not in counter_objek_in:
                                    counter_objek_in[obj_name] = 1
                                else:
                                    counter_objek_in[obj_name] += 1
                            else :
                                counter_out += 1
                                counter_objek_out_keys.append(k)
                                if obj_name not in counter_objek_out:
                                    counter_objek_out[obj_name] = 1
                                else:
                                    counter_objek_out[obj_name] += 1

                                print(counter_objek_out)
                                # r.publish(redis_topic,json.dumps(counter_objek_out))

                            break

        # cv2.imshow('hasil yolov8',frame)
        # if(cv2.waitKey(30)==27):
        #     break
        current_time = time.time()
        if current_time - start_time >= 30:
            print('~~~~~~~~~~~~~~~~~~~~~~~~~')
            print('30 detik')
            print('~~~~~~~~~~~~~~~~~~~~~~~~~')
            start_time = current_time
            for key,value in counter_objek.items():
                # REST-API
                name = key
                count_tmp = value
                api_params = {
                    'camera_id':str(cam_id),
                    'object':name,
                    'counter':str(count_tmp),
                    # 'counter':str(counter_objek[name]),
                }
                req = requests.get(url=http_url+'api/Detectors/log_deteksi',params=api_params)
                # RESET hitungan
                counter_objek[name] = 0

        ret2, frame2 = cv2.imencode('.png', frame)
        # ret2, frame2 = cv2.imencode('.jpg', frame)
        try:
            process.stdin.write(frame2.tobytes())
            process.stdin.flush()
        except Exception as e:
            print('error kirim RTSP',e)
            exit()

        if rekam:
            rekaman.write(frame)
            
            nomor_frame += 1

            current_time_rekaman = time.time()
            waktu_proses = current_time_rekaman - past_time_rekaman
            fps_program = 60 / waktu_proses
            past_time_rekaman = current_time_rekaman

            durasi_rekam_berjalan_waktu = current_time_rekaman - start_time_rekaman
            durasi_rekam_berjalan_frame = nomor_frame / fps
            durasi_rekam_berjalan = durasi_rekam_berjalan_waktu

            print('📹 rekaman berjalan (python time)',durasi_rekam_berjalan_waktu,'detik')
            print('📹 rekaman berjalan (frame count)',durasi_rekam_berjalan_frame,'detik')
            print('⏲️ durasi rekaman adalah',durasi_rekam,'menit (',(durasi_rekam*60),'detik)')
            # print('🖥️ FPS video   :',fps)
            # print('🖥️ FPS program :',fps_program)
            # print('🖥️ waktu proses:',waktu_proses)
            
            if durasi_rekam_berjalan >= (durasi_rekam * 60):
                print('@@@@@@@@@@@@@@@@@@@@@@@@')
                print('TEREKAM',durasi_rekam,'MENIT')
                print('@@@@@@@@@@@@@@@@@@@@@@@@')

                # kirim ke dashboard
                # http://localhost/beacukai_camera/api/IPcamera/record_insert/?camera_id={id_camera}&file_url={URL Records}&duration_sec={Durasi}
                video_out_url = 'http://'+cf.config['device_vpn_ip']+':5000/video_feed?video_path='+FOLDER_OTPUT+'/video_'+str(cam_id)+'_'+str(start_time_rekaman)+'.avi'
                api_params = {
                    'camera_id':str(cam_id),
                    'file_url':video_out_url,
                    'duration_sec':str(int(durasi_rekam_berjalan_frame)),
                    # 'counter':str(counter_objek[name]),
                }
                req = requests.get(url=http_url+'api/IPcamera/record_insert',params=api_params)

                # if 'rekaman' in locals():
                rekaman.release()
                rekaman = None
                rekaman = prepare_rekaman(cam_id, start_time_rekaman, video_w, video_h)
                # exit()
                start_time_rekaman = current_time_rekaman
                nomor_frame = 0

def opt_parse():
    parser = argparse.ArgumentParser(
        description='Program Object detection untuk mendeteksi, mengklasisfikasi, dan menghitung kendaraan dari tangkapan RTSP CCTV'
    )
    parser.add_argument('--id','-i',type=int,default=None,help='id kamera dari tabel kamera di database')
    parser.add_argument('--cam_id','-c',type=int,default=1,help='id kamera dari tabel kamera di server')
    parser.add_argument('--all','-a',type=bool,default=False,help='mendeteksi semua class, tidak hanya '+allowed_cls_str)
    parser.add_argument('--rekam','-r',type=bool,default=False,help='rekam hasil deteksi')
    parser.add_argument('--durasi_rekam','-dr',type=int,default=10,help='durasi rekaman video (jika direkam)')
    opt = parser.parse_args()
    return opt

def main():
    opt = opt_parse()
    if opt.id is not None:
        # run(int(opt.id))
        # run(int(opt.id),opt.all)
        # run(int(opt.id),opt.all,opt.rekam)
        # run(int(opt.id),opt.all,opt.rekam,opt.cam_id)
        run(int(opt.id),opt.all,opt.rekam,opt.cam_id,opt.durasi_rekam)
    else:
        mycursor = mydb.cursor()
        mycursor.execute('select * from kamera')
        myresult = mycursor.fetchall()
        mycursor.close()
        # print(myresult)
        tabel = PrettyTable()
        tabel.field_names = ['id','nama','ip','lokasi','rtsp','cam_id']
        tabel.add_rows(myresult)
        print()
        print('Daftar kamera di database: ')
        print(tabel)
        print()

if __name__=='__main__':
    main()