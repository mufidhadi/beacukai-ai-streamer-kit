import cv2
from ultralytics import YOLO
import supervision as sv
import subprocess as sp
import mysql.connector
import utils.config as cf
import argparse
from prettytable import PrettyTable

# TODO: multi tasking fungsi run

mydb = mysql.connector.connect(
  host=cf.config['mysql']['host'],
  user=cf.config['mysql']['user'],
  password=cf.config['mysql']['password'],
  database=cf.config['mysql']['database'],
)

# koordinat garis counting objek
START = sv.Point(320, 0)
END = sv.Point(320, 480)

rtsp_server = cf.config['rtsp']['base_url']

size_str = '640x480'
fps = 30

def run(id):
    id = str(id)
    rtsp_server_run = rtsp_server+str(id)
    mycursor = mydb.cursor()
    mycursor.execute('select * from kamera where id='+id)
    myresult = mycursor.fetchone()
    print(myresult)
    rtsp_address = myresult[4]
    print('[START!] ambil rtsp:',rtsp_address)

    # ambil ukuran video
    cap = cv2.VideoCapture(rtsp_address)
    video_h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    video_w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    size_str = str(video_w)+'x'+str(video_h)
    fps = cap.get(cv2.CAP_PROP_FPS)

    # bikin start dan end garis jadi horizontal selebar video
    START = sv.Point(0, int(video_h/2))
    END = sv.Point(int(video_w), int(video_h/2))

    command = ['ffmpeg',
        '-re',
        '-s', size_str,
        '-r', str(fps),  # rtsp fps (from input server)
        '-i', '-',
            
        # You can change ffmpeg parameter after this item.
        '-pix_fmt', 'yuv420p',
        '-r', '30',  # output fps
        '-g', '50',
        '-c:v', 'libx264',
        '-b:v', '2M',
        '-bufsize', '64M',
        '-maxrate', "4M",
        '-preset', 'veryfast',
        '-rtsp_transport', 'tcp',
        '-segment_times', '5',
        '-f', 'rtsp',
        rtsp_server_run
    ]

    process = sp.Popen(command, stdin=sp.PIPE)

    model = YOLO(cf.config['yolo']['model'])

    line_zone = sv.LineZone(start=START, end=END)
    line_zone_annotator = sv.LineZoneAnnotator(
        thickness=2,
        text_thickness=1,
        text_scale=0.5
    )

    box_anotator = sv.BoxAnnotator(
        thickness=2,
        text_thickness=1,
        text_scale=0.5
    )

    for result in model.track(source=rtsp_address, show=False, stream=True, tracker="bytetrack.yaml"):
        frame = result.orig_img

        detections = sv.Detections.from_yolov8(result)

        if result.boxes.id is not None:
            detections.tracker_id = result.boxes.id.cpu().numpy().astype(int)
            print('detection:',detections)

        # detections = detections[detections.class_id != 0] #memfilter agar class 0 tidak ditampilkan di frame

            labels = [
                f'id:{tracker_id} {model.model.names[class_id]} ({confidence:0.2f})'
                for _, confidence, class_id, tracker_id
                in detections
            ]

            frame = box_anotator.annotate(scene=frame, detections=detections, labels=labels)

        line_zone.trigger(detections=detections)
        line_zone_annotator.annotate(frame=frame, line_counter=line_zone)

        print('in:',line_zone.in_count)
        print('out:',line_zone.out_count)
        print('state:',line_zone.tracker_state)

        # cv2.imshow('hasil yolov8',frame)
        # if(cv2.waitKey(30)==27):
        #     break

        ret2, frame2 = cv2.imencode('.png', frame)
        try:
            process.stdin.write(frame2.tobytes())
        except:
            print('error kirim RTSP')
        #     exit()

def opt_parse():
    parser = argparse.ArgumentParser(
        description='Program Object detection untuk mendeteksi, mengklasisfikasi, dan menghitung kendaraan dari tangkapan RTSP CCTV'
    )
    parser.add_argument('--id','-i',type=int,default=None,help='id kamera dari tabel kamera di database')
    opt = parser.parse_args()
    return opt

def main():
    opt = opt_parse()
    if opt.id is not None:
        run(int(opt.id))
    else:
        mycursor = mydb.cursor()
        mycursor.execute('select * from kamera')
        myresult = mycursor.fetchall()
        # print(myresult)
        tabel = PrettyTable()
        tabel.field_names = ['id','nama','ip','lokasi','rtsp']
        tabel.add_rows(myresult)
        print()
        print('Daftar kamera di database: ')
        print(tabel)
        print()

if __name__=='__main__':
    main()