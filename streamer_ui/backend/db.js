const mysql = require('mysql');
const fs = require('fs');
const yaml = require('js-yaml');

// Read configuration from config.yaml
const configFile = fs.readFileSync('config.yaml', 'utf8');
const config = yaml.load(configFile);
// MySQL Database Connection
const db = mysql.createConnection({
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
});
db.connect((err) => {
    if (err) {
        console.error('Error connecting to MySQL:', err);
        return;
    }
    console.log('Connected to MySQL');
});
exports.db = db;
