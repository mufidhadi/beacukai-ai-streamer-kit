import React, { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link, Navigate, Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import AdminLteCard from './components/admin_lte_card';
import AdminLte from './components/admin_lte';
import Login from './pages/login';
import Register from './pages/register';
import Home from './pages/home';
import KameraList from './pages/kamera/list';
import FormKamera from './pages/kamera/form';
import DetailKamera from './pages/kamera/detail';
import { DetailCardBootstrap } from './components/detail_card_bootstrap';
import WatchKamera from './pages/kamera/watch';
import AutorunList from './pages/autorun/list';
import AIList from './pages/ai/list';
import AIKamera from './pages/ai/watch';
import AILine from './pages/ai/line';
import AIAdd from './pages/ai/add';

function App() {
  const key_auth_token = 'token'
  const navigate = useNavigate()

  useEffect(() => {
    let authToken = sessionStorage.getItem(key_auth_token)
    // console.log(authToken)

    if (!authToken || authToken==null) {
      navigate('/login')
    }
  }, [])

  const terimaLogin = (token) => {
    sessionStorage.setItem(key_auth_token, token)
    navigate('/')
  }

  return (
    <>
      <ToastContainer />
      <Routes>
        <Route path='/login' element={<Login handleLogin={terimaLogin} />} />
        <Route path='/register' element={<Register handleLogin={terimaLogin} />} />
        <Route path='/' element={<Home />} />
        
        <Route path='/kamera' element={<KameraList />} />
        <Route path='/kamera/add' element={<FormKamera />} />
        <Route path='/kamera/edit/:id' element={<FormKamera tambah_or_edit='edit'/>} />
        <Route path='/kamera/:id' element={<DetailKamera />} />
        <Route path='/kamera/watch/:id' element={<WatchKamera />} />
        
        <Route path='/autorun' element={<AutorunList />} />

        <Route path='/ai' element={<AIList />} />
        <Route path='/ai/watch/:id' element={<AIKamera />} />
        <Route path='/ai/line/:id' element={<AILine />} />
        <Route path='/ai/add' element={<AIAdd />} />

        <Route path='/tes' element={
          <>
            <AdminLte judul='Tes'>
              <div className='container-fluid'>
                <div className='row'>
                  <div className='col-12'>
                    <AdminLteCard tittle='tes fitur'>
                      <DetailCardBootstrap data={
                        {
                          nama:'mufid',
                          tahun_lahir: 1997,
                          tempat_tanggal_lahir: 'purbalingga',
                          bahasa:[
                            'indonesia',
                            'jawa',
                            'inggris',
                            'arab',
                            'jepang',
                          ],
                          anak:[
                            {
                              nama:'mecca',
                              umur:11,
                              bahasa:[
                                'bayi',
                                'indonesia',
                                'jawa',
                              ]
                            },
                            {
                              nama:'medina',
                              umur:0,
                              bahasa:[
                                'bayi',
                                'indonesia',
                                'jawa',
                              ]
                            },
                          ]
                        }
                      } />
                    </AdminLteCard>
                  </div>
                </div>
              </div>
            </AdminLte>
          </>
        }/>
      </Routes>
    </>
  )
}

export default App;
