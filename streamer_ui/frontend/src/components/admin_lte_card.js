import React from "react"

export default function AdminLteCard({ children, tittle = '', header_component, footer_component }) {
    return (
        <>
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title">{tittle}</h3>
                    <div className="card-tools">
                        {header_component}
                    </div>
                </div>

                <div className="card-body">
                    {children}
                </div>

                <div className="card-footer">
                    {footer_component}
                </div>
            </div>
        </>
    )
}