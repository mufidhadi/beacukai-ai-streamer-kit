import React from "react"
import { NavLink, matchPath, useLocation } from "react-router-dom"

export default function Sidemenu() {
    return (
        <>
            <li className="nav-header">MENU</li>

            <NavItem text="beranda" fa_class="fa-home" href="/" />
            <NavItem text="kamera" fa_class="fa-camera" href="/kamera" />
            <NavItem text="autorun" fa_class="fa-play" href="/autorun" />
            <NavItem text="ai" fa_class="fa-microchip" href="/ai" />
        </>
    )
}

function NavItem({ text, href = '#', fa_class = 'fa-link', active = false, children }) {
    return (
        <>
            <li className="nav-item">
                <NavLink to={href} className={'nav-link'} reloadDocument>
                    <i className={"nav-icon fas " + fa_class}></i>
                    <p>
                        {text}
                        {children}
                    </p>
                </NavLink>
                {/* <a href={href} className={"nav-link " + (active ? 'active' : '')}>
                </a> */}
            </li>
        </>
    )
}

function NavItemMenu({ text, href = '#', fa_class = 'fa-link', active = false, open = false, children }) {
    const location = useLocation()
    const isOpened = matchPath(
        location.pathname,
        href
    )
    console.log(location.pathname, href, isOpened)
    return (
        <>
            <li className={"nav-item " + (open || isOpened ? 'menu-open' : '')}>
                <NavLink to={href} className={"nav-link " + (active ? 'active' : '')}>
                    <i className={"nav-icon fas " + fa_class}></i>
                    <p>
                        {text}
                        <i className="right fas fa-angle-left"></i>
                    </p>
                </NavLink>
                <ul className="nav nav-treeview">
                    {children}
                </ul>
            </li>
        </>
    )
}