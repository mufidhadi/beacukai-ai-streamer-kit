import React, { useEffect, useState } from "react";

export function TabelBootstrap({ data = [], key_name = 'id', use_standard_action = true, use_standard_number = true, handleDetail = () => { }, handleEdit = () => { }, handleDelete = () => { }, custom_action_component, show_key = true, null_replacer = '-' }) {
    const [komponen, setKomponen] = useState(<></>)

    useEffect(() => {
        try {
            if (data.length < 1) {
                setKomponen(
                    <>
                        <div className="alert alert-warning" role="alert">
                            Maaf, belum ada data disini
                        </div>
                    </>
                );
            }

            const keys = data.length > 0 ? Object.keys(data[0]) : [];

            let tr_up = [];
            let tr_down = [];
            let values = [];

            keys.map((key) => {
                if (key !== key_name) {
                    if (typeof data[0][key] === 'object') {
                        Object.keys(data[0][key]).map((subKey) => {
                            if (isNaN(parseInt(subKey)))
                                tr_down.push(subKey);
                        });
                    }
                    tr_up.push(key);
                }
            });

            data.map((item) => {
                const row = [];
                keys.map((key) => {
                    if (item[key] == null) {
                        row.push(null_replacer)
                    }
                    if (key !== key_name && item[key] != null) {
                        if (typeof item[key] === 'object') {
                            Object.keys(item[key]).map((subKey) => {
                                row.push(item[key][subKey]);
                            });
                        } else {
                            const val = item[key]
                            row.push(val);
                        }
                    }
                });
                values.push(row);
            });


            const two_tr = tr_down.length > 0;
            setKomponen(
                <Tabel use_standard_number={use_standard_number} show_key={show_key} two_tr={two_tr} key_name={key_name} tr_up={tr_up} data={data} use_standard_action={use_standard_action} custom_action_component={custom_action_component} tr_down={tr_down} values={values} handleDetail={handleDetail} handleEdit={handleEdit} handleDelete={handleDelete} />
            );
        } catch (error) {
            console.log('🪲 ada BUG', error);
            setKomponen(
                <>
                    <div className="alert alert-warning" role="alert">
                        Maaf, belum ada data disini
                    </div>
                </>
            )
        }
    }, [data])
    return komponen
}
function Tabel({ use_standard_number, show_key, two_tr, key_name, tr_up, data, use_standard_action, custom_action_component, tr_down, values, handleDetail, handleEdit, handleDelete }) {
    return (
        <>
            <div className="table-responsive">
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            {use_standard_number || show_key ?
                                <th rowSpan={two_tr ? 2 : 1}>
                                    {show_key ?
                                        <>
                                            {key_name.replaceAll('_', ' ').toUpperCase()}
                                        </>
                                        :
                                        <>
                                            No.
                                        </>}
                                </th>
                                :
                                <></>}
                            {tr_up.map((key, i) => <th key={i} rowSpan={(typeof data[0][key] === 'object' && !Array.isArray(data[0][key])) ? 1 : 2} colSpan={(typeof data[0][key] === 'object' && !Array.isArray(data[0][key])) ? Object.keys(data[0][key]).length : 1}>{key.replaceAll('_', ' ').toUpperCase()}</th>)}
                            {use_standard_action || custom_action_component ?
                                <th rowSpan={two_tr ? 2 : 1}>ACTION</th>
                                :
                                <></>}
                        </tr>
                        {(two_tr) ?
                            <tr>
                                {tr_down.map((key, i) => <th key={i}>{key.replaceAll('_', ' ').toUpperCase()}</th>
                                )}
                            </tr>
                            :
                            <></>}
                    </thead>
                    <tbody>
                        {values.map((item, index) => (
                            <tr key={index}>
                                {use_standard_number || show_key ?
                                    <td>
                                        {show_key ?
                                            <>
                                                {data[index][key_name]}
                                            </>
                                            :
                                            <>
                                                {index + 1}
                                            </>}
                                    </td>
                                    :
                                    <></>}
                                {item.map((subItem, subIndex) => (
                                    <td key={index + '-' + subIndex}>
                                        {subItem}
                                    </td>
                                ))}
                                {use_standard_action ?
                                    <>
                                        <td>
                                            <StandardActionButtons data={data} index={index} key_name={key_name} handleDetail={handleDetail} handleEdit={handleEdit} handleDelete={handleDelete} />
                                        </td>
                                    </>
                                    :
                                    <></>}
                                {custom_action_component ?
                                    <>
                                        <td>
                                            {custom_action_component}
                                        </td>
                                    </>
                                    :
                                    <></>}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}

function StandardActionButtons({ data, index, key_name, handleDetail, handleEdit, handleDelete }) {
    return (
        <>
            <button onClick={(data[index].hasOwnProperty(key_name)) ? () => { handleDetail(data[index][key_name]) } : null} className="btn btn-default">
                <i className="fas fa-eye" />
            </button>
            <button onClick={(data[index].hasOwnProperty(key_name)) ? () => { handleEdit(data[index][key_name]) } : null} className="btn btn-default">
                <i className="fas fa-edit" />
            </button>
            <button onClick={(data[index].hasOwnProperty(key_name)) ? () => { handleDelete(data[index][key_name]) } : null} className="btn btn-default">
                <i className="fas fa-trash" />
            </button>
        </>
    );
}

