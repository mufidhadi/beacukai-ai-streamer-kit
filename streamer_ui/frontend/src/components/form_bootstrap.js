import React, { useEffect, useState } from "react";
import { FormInputBootstrap } from "./form_input_bootstrap";
import './form_bootstrap.css'

const defaultInputList = [
    { type: 'text', label: 'Nama', name: 'nama', id: 'nama', placeholder: 'Masukan nama anda' },
    { type: 'number', label: 'Usia', name: 'usia', id: 'usia', placeholder: 'Berapa usia anda?', addonTextBack: 'Tahun', inputParams: { min: 0, max: 10 } },
    {
        type: 'select', label: 'Jenis Kelamin', name: 'kelamin', id: 'kelamin', options: [
            { value: null, selected: true, label: 'pilih...' },
            { value: 'lk', label: 'Laki-laki' },
            { value: 'pr', label: 'Perempuan' },
        ]
    },
    { type: 'text', label: 'Nama clan', name: 'nama_clan', id: 'nama_clan', placeholder: 'Masukan nama clan anda', disabled: true },
    { type: 'switch', label: 'Aktifkan langganan news letter', name: 'newsletter', id: 'newsletter', value: 'setuju' },
    { type: 'checkbox', label: 'Saya Setuju dengan syarat dan ketentuan yang berlaku', name: 'setuju', id: 'setuju', value: 'setuju' },
]

export function FormBootstrap({ input_list = defaultInputList, method = 'POST', onSubmit=()=>{}, submit_text='Simpan' }) {

    useEffect(() => {
        console.log(input_list);
    }, [input_list])

    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <form onSubmit={onSubmit} method={method} role="form" className="col-12">

                        {input_list.map((inp, i) => (
                            <FormInputBootstrap key={i} {...inp} />
                        ))}

                        <button type="submit" className="btn btn-primary">{submit_text}</button>
                    </form>
                </div>
            </div>
        </>
    )
}
