import React, { useEffect } from "react"

export function FormInputBootstrap({ type = 'text', label, name, id, value = null, placeholder = '', addonTextFront = null, addonTextBack = null, help_text = null, inputParams = {}, options = [], disabled = false, onChangeHandler=()=>{} }) {
    return (
        <div className='mb-3'>
            {
                (type == 'checkbox') ?
                    <div className="form-check">
                        <input className="form-check-input" type={type} id={id} name={name} value={value} {...inputParams} disabled={disabled} onChange={(e) => {
                            onChangeHandler(e);
                        }} />
                        <label className="form-check-label" htmlFor={id}>{label}</label>
                    </div>
                    :
                    (type == 'switch') ?
                        <div className="form-check form-switch">
                            <input className="form-check-input" type="checkbox" role="switch" id={id} name={name} value={value} {...inputParams} disabled={disabled} onChange={(e) => {
                                onChangeHandler(e);
                            }} />
                            <label className="form-check-label" htmlFor={id}>{label}</label>
                        </div>
                        :
                        <>
                            <span htmlFor={id} className="form-label">{label}</span>
                            <div className="input-group">
                                {(addonTextFront == null) ? <></> :
                                    <span className={"input-group-text"}>{addonTextFront}</span>
                                }
                                {/* <div className={"form-floating"}> */}
                                <>
                                    {
                                        (type == 'select') ?
                                            <select className={"form-select"} id={id} name={name} disabled={disabled} onChange={(e) => {
                                                onChangeHandler(e);
                                            }}>
                                                {options.map((opt) => <option {...opt}>{opt.label}</option>)}
                                            </select>
                                            :
                                            <input type={type} className={"form-control"} id={id} placeholder={placeholder} value={value} name={name} {...inputParams} disabled={disabled} onChange={(e) => {
                                                onChangeHandler(e);
                                            }}/>
                                    }
                                    {/* <label htmlFor={id} className={"form-label"}>{label}</label> */}
                                </>
                                {(addonTextBack == null) ? <></> :
                                    <span className={"input-group-text"}>{addonTextBack}</span>
                                }
                            </div>
                        </>
            }
            {(help_text == null) ? <></> :
                <div className="form-text" id={id + '_help'}>{help_text}</div>
            }
        </div>
    )
}