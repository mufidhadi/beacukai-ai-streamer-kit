import { useCallback, useEffect, useRef, useState } from "react"
import styles from './canvas-setting-kamera.module.css'

var garis_deteksi = [
    {
        start: null,
        end: null
    },
    {
        start: null,
        end: null
    },
]

export function CanvasSettingKamera({ gambar = '/ss/thumbnail_3.jpg' }) {

    const canvasRef = useRef(null)
    const [idGaris, setIdGaris] = useState(-1)
    const [garis, setGaris] = useState({})
    const [srcGambar, setSrcgambar] = useState('')
    let click_count = 0

    useEffect(() => {
        setupDraw()
        console.log('GAAASSSS');
    }, [draw, gambar])
    // }, [draw])

    const setupDraw = useCallback(()=>{
        const canvas = canvasRef.current
        const context = canvas.getContext('2d')

        draw(context, canvas, idGaris)
    })

    const updateIdGaris = (id) => {
        // garis_deteksi[id].start = garis_deteksi[id].end = null
        setIdGaris(id)
    }

    function draw(ctx, canvas, idGaris) {
        const full_w = ctx.canvas.width
        const full_h = ctx.canvas.height
        const w = (val) => full_w / 100 * val
        const h = (val) => full_h / 100 * val
        const line_width = 5

        let GD_index = idGaris
        if (GD_index > -1) {
            garis_deteksi[GD_index].start = null
            garis_deteksi[GD_index].end = null
            console.log('🤯 DIHAPUS', garis_deteksi[GD_index])
        }

        ctx.fillStyle = '#00f'
        ctx.fillRect(0, 0, full_w, full_h)
        ctx.fillStyle = '#f00'
        ctx.fillRect(0, 0, w(50), h(50))

        const image = new Image()
        if (gambar !== srcGambar) {
            image.src = gambar
            setSrcgambar(gambar)
        }

        image.onload = () => {
            const scale_factor = Math.min(canvas.width / image.width, canvas.height / image.height)

            const lebar = image.width * scale_factor
            const tinggi = image.height * scale_factor

            ctx.drawImage(image, w(50) - (lebar / 2), h(50) - (tinggi / 2), lebar, tinggi)

            gambarGarisDeteksi()

            canvas.addEventListener('mousedown', (event) => {
                const { y, x } = posisiMouse(event)
                console.log('CLICK CLIK CLICK',click_count, posisiMouse(event));
                click_count++
            })
            canvas.addEventListener('mousemove', (event) => {
                ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
                ctx.drawImage(image, w(50) - (lebar / 2), h(50) - (tinggi / 2), lebar, tinggi)
                gambarGarisDeteksi()

                const { y, x } = posisiMouse(event)

                ctx.strokeStyle = '#000'
                garis(w(0), y, w(100), y)
                garis(x, h(0), x, h(100))

                if (GD_index != -1) {
                    if (garis_deteksi[GD_index].start !== null && garis_deteksi[GD_index].end === null) {
                        ctx.strokeStyle = '#fff'
                        garis(garis_deteksi[GD_index].start.x, garis_deteksi[GD_index].start.y, x, y)
                    } else if (garis_deteksi[GD_index].start !== null && garis_deteksi[GD_index].end !== null) {
                        ctx.strokeStyle = '#fff'
                        garis(garis_deteksi[GD_index].start.x, garis_deteksi[GD_index].start.y, garis_deteksi[GD_index].end.x, garis_deteksi[GD_index].end.y)
                    }
                }
                // teks('id garis hook:' + idGaris, w(50), h(50))
            })
        }


        function gambarGarisDeteksi() {
            ctx.strokeStyle = '#fff'
            for (let i = 0; i < garis_deteksi.length; i++) {
                // if (i != GD_index) {
                const GD = garis_deteksi[i]
                if (GD.start !== null && GD.end !== null) {
                    garis(GD.start.x, GD.start.y, GD.end.x, GD.end.y)
                    const x_tengah = (GD.start.x + GD.end.x) / 2
                    const y_tengah = (GD.start.y + GD.end.y) / 2
                    teks('detektor ' + (i + 1), x_tengah, y_tengah, h(5))
                }
                // }
            }
        }

        function posisiMouse(event) {
            const rect = canvas.getBoundingClientRect()
            const scale_factor = Math.min(canvas.width / rect.width, canvas.height / rect.height)

            const y = (event.clientY - rect.top) * scale_factor
            const x = (event.clientX - rect.left) * scale_factor
            return { y, x }
        }

        function teks(tks, x, y, size = null) {
            ctx.font = ((size == null) ? h(10) : size) + 'px monospace'
            ctx.fillStyle = 'white'
            ctx.textAlign = 'center'
            ctx.textBaseline = 'middle'

            const txt_w = ctx.measureText(tks).width + (((size == null) ? h(10) : size) / 2)
            const txt_h = ((size == null) ? h(10) : size)
            ctx.fillStyle = '#fa4b2a'
            ctx.fillRect((x - (txt_w / 2)), (y - (txt_h / 2)), txt_w, txt_h)

            ctx.beginPath()

            ctx.fillStyle = '#fff'
            ctx.fillText(tks, x, y)
        }

        function garis(x0, y0, x1, y1) {
            ctx.beginPath()
            ctx.moveTo(x0, y0)
            ctx.lineTo(x1, y1)
            ctx.lineWidth = line_width
            ctx.stroke()
        }
    }

    return (
        <>
            {/* <pre>id garis: {idGaris}</pre> */}
            <div className={styles.canvas_container}>
                <canvas ref={canvasRef} className={styles.canvas} width={1920} height={1080} />
            </div>
        </>
    )
}