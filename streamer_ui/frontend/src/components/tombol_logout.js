import React from "react";
import { useNavigate } from "react-router-dom";

export function TombolLogout({ className = 'btn btn-danger' }) {
    const key_auth_token = 'token'
    const navigate = useNavigate()

    const handleLogout = () => {
        sessionStorage.removeItem(key_auth_token);
        navigate('/login')
    }

    return (
        <>
            <button type="button" className={className} onClick={handleLogout}>Logout</button>
        </>
    )
}