// src/frontend-config.js
const fs = require('fs');
const yaml = require('js-yaml');

try {
    const configFile = fs.readFileSync('config.yaml', 'utf8');
    const config = yaml.load(configFile);

    // const frontendConfig = {
    //     apiBaseUrl: config.api.baseUrl
    // }

    // module.exports = frontendConfig;
    module.exports = config
} catch (e) {
    console.error('Error reading frontend config file:', e);
}
