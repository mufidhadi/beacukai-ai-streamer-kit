import React, { useState } from "react"
import { Link } from "react-router-dom"
import './login.css'
import axios from "axios"
import { toast } from "react-toastify"

export default function Register({ handleLogin }) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const response = await axios.post('http://'+window.location.hostname+':3334/auth/register', {
                username,
                password
            });

            if (response.data.token) {
                handleLogin(response.data.token);
            } else {
                toast.error('username ini sudah ter-daftar', {
                    position: toast.POSITION.TOP_CENTER
                })
            }
        } catch (error) {
            console.error('Login error:', error);
            toast.error('Tidak dapat mendaftar, terdapat masalah pada sistem kami', {
                position: toast.POSITION.TOP_CENTER
            })
        }
    }
    return (
        <>
            <div id="main-wrapper" className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-10">
                        <div className="card border-0">
                            <div className="card-body p-0">
                                <div className="row no-gutters">
                                    <div className="col-lg-6">
                                        <div className="p-5">
                                            <div className="mb-5">
                                                <h3 className="h4 font-weight-bold text-theme">Register</h3>
                                            </div>
                                            <h6 className="h5 mb-0">Welcome!</h6>
                                            <p className="text-muted mt-2 mb-5">
                                                Enter a new username and password to register.
                                            </p>
                                            <form onSubmit={handleSubmit}>
                                                <div className="form-group">
                                                    <label htmlFor="inputUsername">Username</label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        id="inputUsername"
                                                        value={username}
                                                        onChange={(e) => setUsername(e.target.value)}
                                                    />
                                                </div>
                                                <div className="form-group mb-5">
                                                    <label htmlFor="inputPassword">Password</label>
                                                    <input
                                                        type="password"
                                                        className="form-control"
                                                        id="inputPassword"
                                                        value={password}
                                                        onChange={(e) => setPassword(e.target.value)}
                                                    />
                                                </div>
                                                <button type="submit" className="btn btn-theme">
                                                    Register
                                                </button>
                                                {/* <a href="#l" className="forgot-link float-right text-primary">
                                                    Forgot password?
                                                </a> */}
                                            </form>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 d-none d-lg-inline-block">
                                        <div className="account-block rounded-right">
                                            <div className="overlay rounded-right" />
                                            <div className="account-testimonial">
                                                <h4 className="text-white mb-4">
                                                    Admin AI-Streamer
                                                </h4>
                                                {/* <p className="lead text-white">
                                                    "Best investment i made for a long time. Can only recommend
                                                    it for other users."
                                                </p>
                                                <p>- Admin User</p> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* end card-body */}
                        </div>
                        {/* end card */}
                        <p className="text-muted text-center mt-3 mb-0">
                            Already have an account?{" "}
                            <Link to={'/login'} className="text-primary ml-1">
                                login
                            </Link>
                        </p>
                        {/* end row */}
                    </div>
                    {/* end col */}
                </div>
                {/* Row */}
            </div>

        </>
    )
}