import React, { useEffect, useState } from "react"
import AdminLte from "../../components/admin_lte"
import AdminLteCard from "../../components/admin_lte_card"
import axios from "axios"
import { TabelBootstrap } from "../../components/TabelBootstrap"
import { Link, useNavigate } from "react-router-dom"
import { TombolWatch } from "../../components/tombol_watch"

export default function KameraList({ }) {
    const navigate = useNavigate()
    const [data, setData] = useState([])
    useEffect(() => {
        updateData()
    }, [])

    function updateData() {
        const token = sessionStorage.getItem('token'); // Retrieve the token from storage
        // console.log('🖥️',token);

        if (!token) {
            // Handle not authenticated
            return;
        }

        // Make an authenticated request to the secured route
        axios.get('http://'+window.location.hostname+':3334/kamera', {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {

                axios.get('http://'+window.location.hostname+':5555/list')
                    .then((response2) => {
                        const dataPm2 = response2.data.processes
                        let newData = response.data

                        newData.map((dt, i) => {
                            const id = dt.id

                            let status_pm2 = ''

                            dataPm2.map((pm2) => {
                                if (pm2.name == 'cam' + id) {
                                    status_pm2 = pm2.pm2_env.status
                                }
                            })

                            newData[i].rtsp = [
                                <>
                                    {dt.rtsp}
                                    &ensp;
                                    <TombolWatch id={id} />
                                </>
                            ]
                            newData[i].ai_service = [
                                <>
                                    {(status_pm2 !== '') ?
                                        <>
                                            {status_pm2}
                                        </>
                                        :
                                        <button className="btn btn-default" type="button" onClick={() => {addAI(id)}}>
                                            <i className="fas fa-plus" />
                                            &ensp;
                                            Activate
                                        </button>
                                    }
                                </>
                            ]
                        })
                        setData(response.data);
                    })
                    .catch((error2) => {
                        console.error('Error fetching data pm2:', error2);
                    });

            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            });
    }

    function handleDetail(id) {
        console.log('Detail 👉', id)
        navigate('./' + id, { relative: 'path' })
    }

    function handleEdit(id) {
        console.log('Edit 👉', id)
        navigate('./edit/' + id, { relative: 'path' })
    }

    function handleDelete(id) {
        const token = sessionStorage.getItem('token')

        console.log('Delete 👉', id)

        axios.delete('http://'+window.location.hostname+':3334/kamera/' + id, {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {
                setData(dataLama => {
                    let dataBaru = []
                    dataLama.map((dl, i) => {
                        if (dl.id != id) {
                            dataBaru.push(dl)
                        }
                    })
                    return dataBaru
                })
                // console.log(input_list_template,response.data)
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            });
    }

    function addAI(id) {
        const token = sessionStorage.getItem('token')

        console.log('activate AI 👉', id)

        axios.get('http://'+window.location.hostname+':5555/create/' + id)
            .then((response) => {
                updateData()
                // console.log(input_list_template,response.data)
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            });
    }

    return (
        <>
            <AdminLte judul='Kamera'>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-12'>
                            <AdminLteCard
                                tittle='Home'
                                header_component={
                                    <>
                                        <Link to='./add' relative="path" className="btn btn-primary">
                                            <i className="fas fa-plus"></i>
                                            &ensp;
                                            Tambah data
                                        </Link>
                                    </>
                                }
                            >
                                <TabelBootstrap data={data} handleEdit={handleEdit} handleDetail={handleDetail} handleDelete={handleDelete} show_key={false} />
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}