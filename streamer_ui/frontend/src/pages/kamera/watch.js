import React, { useEffect, useState } from "react"
import AdminLte from "../../components/admin_lte"
import AdminLteCard from "../../components/admin_lte_card"
import { TombolKembali } from "../../components/tombol_kembali"
import { Link, useNavigate, useParams } from "react-router-dom"
import axios from "axios"

export default function WatchKamera({ }) {
    const navigate = useNavigate()
    const { id } = useParams()

    // const mjpegUrlTemplate = 'http://'+window.location.hostname+':5000/video_feed?rtsp_url='
    const mjpegUrlTemplate = 'http://'+window.location.hostname+':5000/video_feed?video_path='

    const [data, setData] = useState({})
    const [mjpegUrl, setMjpegUrl] = useState('')

    useEffect(() => {
        const token = sessionStorage.getItem('token')

        axios.get('http://'+window.location.hostname+':3334/kamera/' + id, {
            headers: {
                Authorization: token
            }
        })
            .then((response) => {
                setData(response.data)
                setMjpegUrl(mjpegUrlTemplate + response.data.rtsp)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }, [])

    return (
        <>
            <AdminLte judul="Lihat RTSP">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <AdminLteCard
                                tittle="RTSP"
                                header_component={
                                    <>
                                        <TombolKembali />
                                        &ensp;
                                        <TombolDetail id={id} />
                                        &ensp;
                                        <TombolEdit id={id} />
                                    </>
                                }
                                footer_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }
                            >
                                <div className="row">
                                    <div className="col-12">
                                        <img className="img-fluid mx-auto d-block" lazy={true} src={mjpegUrl} />
                                    </div>
                                </div>
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}

function TombolDetail({ id }) {
    return (
        <>
            <Link to={'/kamera/' + id} className="btn btn-default">
                <i className="fas fa-eye" />
                &ensp;
                Detail
            </Link>
        </>
    )
}

function TombolEdit({ id }) {
    return (
        <>
            <Link to={'/kamera/edit/' + id} className="btn btn-default">
                <i className="fas fa-edit" />
                &ensp;
                Edit
            </Link>
        </>
    )
}