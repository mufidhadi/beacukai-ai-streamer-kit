import React, { useEffect, useState } from "react"
import AdminLte from "../../components/admin_lte"
import AdminLteCard from "../../components/admin_lte_card"
import { TombolKembali } from "../../components/tombol_kembali"
import { Link, useNavigate, useParams } from "react-router-dom"
import axios from "axios"

export default function AIKamera({ }) {
    const navigate = useNavigate()
    const { id } = useParams()

    const rtsp_url_template = 'rtsp://'+window.location.hostname+':8554/ai/'
    // const mjpegUrlTemplate = 'http://'+window.location.hostname+':5000/video_feed?rtsp_url=' + rtsp_url_template + id
    const mjpegUrlTemplate = 'http://'+window.location.hostname+':5000/video_feed?video_path=' + rtsp_url_template + id


    const [data, setData] = useState({})
    const [mjpegUrl, setMjpegUrl] = useState(mjpegUrlTemplate)

    useEffect(() => {
        return ()=>{
            setMjpegUrl('')
        }
    }, [])

    return (
        <>
            <AdminLte judul="Lihat RTSP">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <AdminLteCard
                                tittle="RTSP"
                                header_component={
                                    <>
                                        <TombolKembali />
                                        {/* &ensp;
                                        <TombolDetail id={id} />
                                        &ensp;
                                        <TombolEdit id={id} /> */}
                                        &ensp;
                                        <Link to={'../../line/' + id} relative="path" className="btn btn-default" >
                                            <i className="fas fa-edit" />
                                            &ensp;
                                            Line Edit
                                        </Link>
                                    </>
                                }
                                footer_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }
                            >
                                <div className="row">
                                    <div className="col-12">
                                        <img className="img-fluid mx-auto d-block" lazy={true} src={mjpegUrl} />
                                    </div>
                                </div>
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}

function TombolDetail({ id }) {
    return (
        <>
            <Link to={'/kamera/' + id} className="btn btn-default">
                <i className="fas fa-eye" />
                &ensp;
                Detail
            </Link>
        </>
    )
}

function TombolEdit({ id }) {
    return (
        <>
            <Link to={'/kamera/edit/' + id} className="btn btn-default">
                <i className="fas fa-edit" />
                &ensp;
                Edit
            </Link>
        </>
    )
}