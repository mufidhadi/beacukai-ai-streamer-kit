import axios from "axios"
import React, { useEffect, useState } from "react"
import AdminLte from "../../components/admin_lte"
import { FormBootstrap } from "../../components/form_bootstrap"
import AdminLteCard from "../../components/admin_lte_card"
import { TombolKembali } from "../../components/tombol_kembali"
import { toast } from "react-toastify"
import { useNavigate } from "react-router-dom"

export default function AIAdd() {
    const navigate = useNavigate()

    const [data, setData] = useState([])
    const [inputList, setInputList] = useState([])
    const [optKamera, setOptKamera] = useState([])
    const [isian, setIsian] = useState({})

    useEffect(() => {
        const token = sessionStorage.getItem('token'); // Retrieve the token from storage
        // console.log('🖥️',token);

        if (!token) {
            // Handle not authenticated
            return;
        }

        // Make an authenticated request to the secured route
        axios.get('http://'+window.location.hostname+':3334/kamera', {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {
                setData(response.data)
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            })
    }, [])

    useEffect(() => {
        updateInputList()
    }, [data])

    function updateInputList() {
        let inpt_list_template = [
            {
                type: 'hidden',
                name: 'id',
                value: null,
            },
            {
                type: 'select',
                label: 'kamera',
                name: 'cam_id',
                id: 'cam_id',
                value: null,
                options: [
                    { value: null, selected: true, label: 'pilih...' },
                ],
            },
        ]

        data.map((dat) => {
            inpt_list_template[1].options.push({
                value: dat.id,
                label: dat.nama,
            })
        })

        inpt_list_template.map((input, i) => {
            inpt_list_template[i] = {
                ...input, onChangeHandler: (e) => {
                    setIsian(isiLama => {
                        return { ...isiLama, [e.target.name]: e.target.value }
                    })
                    setInputList(inputListLama => {
                        let inputListBaru = []
                        inputListLama.map((il, i) => {
                            const il_temp = il
                            if (il.name == e.target.name) {
                                il_temp.value = e.target.value
                            }
                            inputListBaru.push(il_temp)
                        })
                        return inputListBaru
                    })
                }
            }
        })

        setInputList(inpt_list_template)
    }

    async function onSubmitHandler(e) {
        e.preventDefault()

        try {
            if (isian.cam_id) {
                axios.get('http://'+window.location.hostname+':5555/create/' + isian.cam_id)
                    .then((response) => {
                        console.log(response)
                        toast('proses AI berhasil dibuat')
                        navigate(-1)
                    })
            }
        } catch (error) {
            console.log('🪲 ERROR CUY:', error)
        }
    }

    return (
        <>
            <AdminLte judul="AI">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <AdminLteCard
                                tittle="Add"
                                header_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }
                                footer_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }>
                                {JSON.stringify(isian)}
                                <FormBootstrap input_list={inputList} submit_text="Aktifkan" onSubmit={onSubmitHandler} />
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}