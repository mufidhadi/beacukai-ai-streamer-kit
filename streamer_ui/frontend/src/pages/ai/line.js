import React, { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import AdminLte from "../../components/admin_lte"
import AdminLteCard from "../../components/admin_lte_card"
import { TombolKembali } from "../../components/tombol_kembali"
import axios from "axios"
import { CanvasSettingKamera } from "../../components/canvas-setting-kamera"

export default function AILine({ }) {
    const navigate = useNavigate()
    const {id} = useParams()

    // const rtsp_url_template = 'rtsp://'+window.location.hostname+':8554/ai/'
    // const mjpegUrlTemplate = 'http://'+window.location.hostname+':5000/video_feed?rtsp_url=' + rtsp_url_template + id
    const mjpegUrlTemplate = 'http://'+window.location.hostname+':5000/video_feed?rtsp_url='


    const [data, setData] = useState({})
    const [mjpegUrl, setMjpegUrl] = useState(mjpegUrlTemplate)

    useEffect(() => {
        updateData()
    }, [])

    function updateData() {
        const token = sessionStorage.getItem('token')

        axios.get('http://'+window.location.hostname+':3334/kamera/' + id, {
            headers: {
                Authorization: token
            }
        })
            .then((response) => {
                if (response.data) {
                    setData(response.data)
                    if (response.data.rtsp) {
                        setMjpegUrl(mjpegUrlTemplate+response.data.rtsp)
                    }
                }
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    return (
        <>
            <AdminLte judul="AI">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <AdminLteCard
                                tittle="Line Editor"
                                header_component={
                                    <>
                                        <TombolKembali />
                                    </>
                                }
                            >
                                {(data.rtsp) ?
                                    <>
                                        <CanvasSettingKamera gambar={mjpegUrl} />
                                    </>
                                    :
                                    <></>
                                }
                            </AdminLteCard>
                        </div>
                    </div>
                </div>
            </AdminLte>
        </>
    )
}