import React, { useEffect, useState } from "react"
import AdminLte from "../components/admin_lte"
import AdminLteCard from "../components/admin_lte_card"
import { SmallBox } from "../components/info_box"
import axios from "axios"
import { BasicChart, DonutChart, LineChart, PieChart } from "../components/chart"
import { sortDataByName } from "../utils/sorter"
import { byteKeUkuranData } from "../utils/konversi"
import { DetailBasicBootstrap } from "../components/detail_basic_bootstrap"
import { TabelBootstrap } from "../components/TabelBootstrap"

export default function Home() {
    const [jumlahKamera, setJumlahKamera] = useState(0)
    const [jumlahAutorun, setJumlahAutorun] = useState(0)
    const [jumlahAI, setJumlahAI] = useState(0)
    const [memData, setMemData] = useState([])
    const [autorunData, setAutorunData] = useState([])
    const [memDataFull, setMemDataFull] = useState({})

    let ws = null
    let closing_pindah_halaman = false

    useEffect(() => {
        const token = sessionStorage.getItem('token'); // Retrieve the token from storage

        if (!token) {
            // Handle not authenticated
            return;
        }

        // Make an authenticated request to the secured route
        updateJumlahKamera(token)
        updateJumlahAutorun(token)
        closing_pindah_halaman = false
        setupWebSocket()

        return () => {
            console.log('💀💀💀💀💀💀💀');
            if (ws!==null) {
                console.log('Cleaning up WebSocket connection');
                closing_pindah_halaman = true
                ws.close();
            }
        }
    }, [])

    return (
        <>
            <AdminLte judul='Home'>
                <div className='container-fluid'>
                    <div className="row">
                        <div className="col-sm-6 col-md-4">
                            <SmallBox fa_icon="fa-camera" href="/kamera" title="Kamera CCTV" number={jumlahKamera} />
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <SmallBox fa_icon="fa-play" href="/autorun" title="autorun process" number={jumlahAutorun} />
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <SmallBox fa_icon="fa-microchip" href="/ai" title="AI service" number={jumlahAI} />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6">
                            <AdminLteCard tittle="autorun sistem penunjang AI">
                                {/* {JSON.stringify(autorunData)} */}
                                <TabelBootstrap data={sortDataByName(autorunData)} show_key={false} use_standard_action={false} />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="penggunaan memory sistem (byte)">
                                {/* {JSON.stringify(memData)} */}
                                <BasicChart type="bar" data={memData} data_label="name" data_value="memory_usage" />
                            </AdminLteCard>
                        </div>
                    </div>
                    {/* <div className="row">
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="scatter" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="line" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="bar" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="radar" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="pie" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="doughnut" />
                            </AdminLteCard>
                        </div>
                        <div className="col-md-6">
                            <AdminLteCard tittle="chart">
                                <BasicChart type="polarArea" />
                            </AdminLteCard>
                        </div>
                    </div> */}

                    {/* <div className='row'>
                        <div className='col-12'>
                            <AdminLteCard tittle='Home'>
                                <h1>Hello, world!</h1>
                            </AdminLteCard>
                        </div>
                    </div> */}
                </div>
            </AdminLte>
        </>
    )

    function updateJumlahKamera(token) {
        axios.get('http://'+window.location.hostname+':3334/kamera', {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {
                setJumlahKamera(response.data.length)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    function updateJumlahAutorun(token) {
        axios.get('http://'+window.location.hostname+':5555/list', {
            headers: {
                Authorization: token // Include the JWT token in the headers
            }
        })
            .then((response) => {
                setJumlahAutorun(response.data.processes.length)
                let jumlah_autorun_ai = 0
                response.data.processes.map((pm2) => {
                    if (pm2.name.includes('cam')) {
                        jumlah_autorun_ai += 1
                    }
                })
                setJumlahAI(jumlah_autorun_ai)
            })
            .catch((error) => {
                console.error('Error fetching data:', error)
            })
    }

    function setupWebSocket() {
        // const ws = new WebSocket('ws://'+window.location.hostname+':5556/')
        ws = new WebSocket('ws://'+window.location.hostname+':5556/')

        ws.onopen = () => {
            console.log('WebSocket connection opened');
        };

        ws.onmessage = (event) => {
            const dataFromServer = JSON.parse(event.data);
            // console.log('Received data from WebSocket:', dataFromServer);

            // Update the data when WebSocket data is received
            if (dataFromServer.processes) {
                const pm2Data = dataFromServer;
                updateAutorunData(pm2Data)
            }

            if (dataFromServer.memory_usage) {
                // setMemData(dataFromServer.memory_usage)
                setMemData(sortDataByName(dataFromServer.memory_usage))
            }
        };

        ws.onclose = () => {
            console.log('WebSocket connection closed');
            if (!closing_pindah_halaman)
            setupWebSocket()
        };

        ws.onerror = (error) => {
            console.error('WebSocket error:', error);
        };

        // setWs(ws)
    }

    function updateAutorunData(pm2_data) {
        let newData = []
        pm2_data.processes.map((pm2_process, i) => {
            newData.push({
                id: pm2_process.pm_id,
                name: pm2_process.name,
                // id_kamera: (pm2_process.name.includes('cam')) ? pm2_process.name.replace('cam', '') : '',
                status: pm2_process.pm2_env.status,
                // cpu: pm2_process.monit.cpu,
                // memory:pm2_process.monit.memory,
                memory: byteKeUkuranData(pm2_process.monit.memory),
                // uptime:pm2_process.pm2_env.pm_uptime,
                // uptime:detikKeWaktu(pm2_process.pm2_env.pm_uptime),
                jumlah_restart: pm2_process.pm2_env.restart_time,
            })
        })
        setAutorunData(newData)
    }
}